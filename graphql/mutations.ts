import { gql } from 'apollo-server';

export default gql`

type Mutation {  	
  	likePost(postId: String!): Boolean
  	
  	updateProfile(userId: String!, input: UserInput!): Boolean
  	
  	addPost(input: PostInput): Post
  	editPost(postId: String!, input: PostInput): Post
}

`;
