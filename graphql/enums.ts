import { gql } from 'apollo-server';

export default gql`
  
  enum Gender {
    MALE
    FEMALE
  }
    
  enum UserType {
    PRO
    AMATEUR
  }
`;
