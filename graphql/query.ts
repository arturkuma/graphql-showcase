import { gql } from 'apollo-server';

export default gql`

type Query {
    getUser(userId: Int!): User
    
    getPost(postId: Int!): Post
    getPosts(userId: Int!, pagination: Pagination): [Post]
    
    getComments(postId: Int!, pagination: Pagination): [Comment]
    
    searchPosts(search: String, pagination: Pagination): [Post]
    searchUsers(search: String, pagination: Pagination): [User]
}

`;
