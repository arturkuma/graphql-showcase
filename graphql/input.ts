import { gql } from 'apollo-server';

export default gql`

  input Pagination {
  	page: Int
  	limit: Int
  }

  input UserInput {
    coverPhotoId: String,
    description: String,
    city: String,
    type: UserType,
    gender: Gender
  }
    
  input PostInput {
    fileId: String!,
    description: String,
    submitToCompetition: Boolean
  }  
    
`;
