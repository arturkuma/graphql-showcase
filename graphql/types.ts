import { gql } from 'apollo-server';

export default gql`
  
  type User {
    id: Int!,
    username: String!,
    email: String!,
    postsCount: Int!,
    posts(pagination: Pagination): [Post]
  }

  type Comment {
    id: Int!,
    user: User!,
    content: String!,
    createdAt: String
  }

  type Post {
    id: Int!,
    description: String,
    createdAt: String!,
    user: User,
    likesCount: Int!,
    lastComments(limit: Int): [Comment],
    isLikedByCurrentUser: Boolean!
  }
  
`;
